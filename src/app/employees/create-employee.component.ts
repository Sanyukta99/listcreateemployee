import { Component, OnInit } from '@angular/core';
import {NgForm } from '@angular/forms';
import { Employee } from '../models/employee.model';
import { EmployeeService } from '../employees/employee.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  private count =Object.keys(this._employeeService.getEmployees()).length;
  panelTitle: string;
   employee: Employee =
  { 
    id:this.count+1,
    name:null,
    city:null,
    gender:null,
    department:null
  };

  constructor(private _employeeService:EmployeeService,private _router:Router,private _route:ActivatedRoute) { }

  ngOnInit() {
    this._route.paramMap.subscribe(parameterMap =>
       {
     const id = +parameterMap.get('id');
     this.getEmployee(id);

    })
  }
  private getEmployee(id:number)
  {
    if(id ==0)
    {
      this.employee={
        id:this.count+1,
       name:null,
       city:null,
       gender:null,
      department:null
    };
    this.panelTitle='Create employee';
    }else{
      this.panelTitle='Edit employee';
      this.employee=Object.assign({},this._employeeService.getEmploye(id));
    }

  }
   saveEmployee(id:number): void 
   {
     if(id==0)
     {
     this._employeeService.save(this.employee);
     this._router.navigate(['list']);
     }else{
       this._employeeService.editor(this.employee);
       this._router.navigate(['list']);
     }


  //  // console.log(empForm.value);
   }

}
