import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee.model';
import { EmployeeService } from './employee.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
 private employees:Employee[];

  constructor(private _employeeService: EmployeeService,private _router: Router){ }
  

  //this ailejunobjectmakam grdaicha 

  ngOnInit()
   {
     
   this.employees = this._employeeService.getEmployees();
   console.log(this.employees);
    
  
    // uhtabta ako data yei classko objma rakheko
   // this.employees =this._employeeService.getList();

    
  }
  delete(id:number)
  {
    this._employeeService.deleteEmployee(id);
  }
  edit(id:number)
  {
  this._router.navigate(['/edit',id]);
  }

}
