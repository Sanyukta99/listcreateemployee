import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Employee } from '../models/employee.model';
import { Observable } from 'rxjs';

import { employeeJson } from '../../assets/employee.js';
@Injectable()
export class EmployeeService
{
    private emplist:Employee[]=employeeJson;
         
    
    constructor(){
      
    }
   
       public getEmployees()
        {
          
           return this.emplist;
          

        }
        public getEmploye(id:number)
        {
            return this.emplist.find(x => x.id === id);
        }
   
         save(employee: Employee)
         {
             
             this.emplist.push(employee);
         }
         deleteEmployee(id:number)
         {
          const i= this.emplist.findIndex(e => e.id == id);
          if(i!==-1) 
          {
              this.emplist.splice(i,1);
          }
         }
         editor(employee:Employee)
         {
             const foundIndex=this.emplist.findIndex(e=> e.id == employee.id);
             this.emplist[foundIndex]=employee;
         }
    
}