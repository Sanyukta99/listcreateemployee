import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes} from '@angular/router';//componentharu bich communicationkolagi
import { HttpClientModule} from '@angular/common/http';
import { EmployeeService} from "./employees/employee.service";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule } from '@angular/forms';
import { ListEmployeesComponent } from './employees/list-employees.component';
import { CreateEmployeeComponent } from './employees/create-employee.component';
const appRoutes: Routes = [
  { path: 'list', component: ListEmployeesComponent},
  { path: 'edit/:id', component: CreateEmployeeComponent},
  { path: '', redirectTo: '/list', pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    ListEmployeesComponent,
    CreateEmployeeComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,FormsModule,
    RouterModule.forRoot(appRoutes)
    
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
